# Selenium

Herramienta que permite el acceso al navegador por medio de código.

Se puede usar para pruebas en las interfaces o hacer web scrapping.


## Driver
Para poder usar **Selenium** se debe hacer uso del driver del navegador y sistema operativo que se pretenda usar.

> [Documentación Oficial](https://www.selenium.dev/documentation/getting_started/installing_browser_drivers/)

Drivers disponibles

- [Chrome](https://chromedriver.chromium.org/downloads)  
*El driver deberá corresponder a la versión de chrome instalada en el equipo.*

- [FireFox](https://github.com/mozilla/geckodriver/releases)

- [Edge](https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/)

- [IExplorer](https://www.selenium.dev/downloads/)

- [Opera](https://github.com/operasoftware/operachromiumdriver/releases)