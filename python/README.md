# Python ussage

## Instalación
Se recomienda usar en untorno virtual con ayuda de *venv* para instalar las dependencias descritas en requirements.txt

```
$ python3 -m venv env
$ source env/bin/activate
$(env) pip install -r requirements.txt
```

*Para correr cara ejercicio debera tener los drivers en un dictorio superior a este o cambiar las rutas en el código.* 

<br>

> 1- HelloWorld

Ejemplo básico de la instancia de selenium y apertura de un navegador.
```
$(env) python ./1-HelloWorld
```