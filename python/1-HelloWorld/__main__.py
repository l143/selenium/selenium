import unittest
from  pyunitreport import HTMLTestRunner

from selenium import webdriver


class HelloWorld(unittest.TestCase):
  def setUp(self):
    self.driver = webdriver.Chrome(executable_path=r'../chromedriver')
    # self.driver = webdriver.Firefox(executable_path=r'../geckodriver')
    self.driver.implicitly_wait(10)

    return super().setUp()

  def tearDown(self):
    self.driver.quit()

  def test_hello(self):
    self.driver.get('https://www.google.com')
    self.driver.implicitly_wait(10)


if __name__ == "__main__":
  unittest.main(
    verbosity=2,
    testRunner=HTMLTestRunner(output='reportes', report_name='Hello World'))